# Built-in library that allows usage of queues
from queue import Queue

class Graph:
	# Constructor
    def __init__(self, num_of_nodes, directed=True): 
        self.m_directed = directed

        self.m_adj_list = {node: list() for node in num_of_nodes}  
			
    # Add edge to the graph
    def add_edge(self, node1, node2, weight=1):
        self.m_adj_list[node1].append((node2, weight))

        if not self.m_directed:
            self.m_adj_list[node2].append((node1, weight))
    
    # Print the graph representation
    def print_adj_list(self):
      for key in self.m_adj_list.keys():
        print("node", key, ": ", self.m_adj_list[key])

    # BSF(Breadth-First Search) algorithm
    def bfs_traversal(self, start_node):
        visited = set() #setting/Initializing an empty array for visited nodes
        queue = Queue() #Initializing a queue for the remaining nodes

        queue.put(start_node) #puts statr_node inside the queue
        visited.add(start_node) #Also adds start_node at the 'visited' list

        while not queue.empty():
            current_node = queue.get() #Returns the first value in a queue

            print(current_node, end=" ") #Prints the value of current_node

    		# Loops through the list and puts the next_node in the queue and vice versa
            for(next_node, weight) in self.m_adj_list[current_node]:
                if next_node not in visited:
                    queue.put(next_node)
                    visited.add(next_node)

    # DSF(Depth-First Search) algorithm
    def dfs(self, start, target, path=[], visited=set()):
        path.append(start) #here start denoting the start node

        visited.add(start)

    	# Checks if the start and target nodes are the same, if so, returns the
        if start == target:
             return path

    	# Loops through the whole graph/list and adds each node within the path
        for(neighbour, weight) in self.m_adj_list[start]:
            if neighbour not in visited:
                result = self.dfs(neighbour, target, path, visited)

                if result is not None:
                    return result

        path.pop()
        self.itr += 1

        return None

n = [0, 1, 2, 3, 4]

graph = Graph(n, directed=False)

graph.add_edge(0, 1)
graph.add_edge(0, 2)
graph.add_edge(1, 2)
graph.add_edge(1, 4)
graph.add_edge(2, 3)

graph.print_adj_list()

# Invoking the bfs_traversal() function
graph.bfs_traversal(0)

print('')
# Invoking the dfs() function
print(graph.dfs(0, 3)) #dfs(start node, target node)