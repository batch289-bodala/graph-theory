# [SECTION] Edge List
class Graph:
	# For initializing the graph as a directed graph
	def __init__(self, directed=True):
		self.m_directed = directed

		self.m_list_of_edges = []

	# For adding 2 vertices and an edge weight to the list of edges, regardless if the graph is directed or not
	def add_edge(self, node1, node2, weight=1):
		self.m_list_of_edges.append([node1, node2, weight])

		if not self.m_directed:
			self.m_list_of_edges.append([node1, node2, weight])

	# For printing the edge list by looping through each value
	def print_edge_list(self):
		for i in range(len(self.m_list_of_edges)):
			print("edge ", i+1, ": ", self.m_list_of_edges[i])

graph = Graph()

graph.add_edge(0, 0, 25)
graph.add_edge(0, 1, 5)
graph.add_edge(0, 2, 3)
graph.add_edge(1, 3, 1)
graph.add_edge(1, 4, 15)
graph.add_edge(4, 2, 7)
graph.add_edge(4, 3, 11)

graph.print_edge_list()


# [SECTION] Adjacency Matrix
import pandas as pd 

class Graph:
	def __init__(self, num_of_nodes, n, directed=True):
		self.m_num_of_nodes = num_of_nodes
		self.m_directed = directed

		self.m_adj_matrix = [[0 for column in range(num_of_nodes)] 
							for row in range(num_of_nodes)]

		# Using DataFrame to present the graph in a form of a table
		self.adj_df = pd.DataFrame(self.m_adj_matrix, columns=[c for c in n], index=[r for r in n])

	def add_edge(self, node1, node2, weight=1):
		self.adj_df[node2][node1] = weight

		if not self.m_directed:
			self.adj_df[node1][node2] = weight

	def print_adj_matrix(self):
		print(self.adj_df)

n = [0, 1, 2, 3, 4]

graph = Graph(5, n, directed=True)

graph.add_edge(0, 0, 25)
graph.add_edge(0, 1, 5)
graph.add_edge(0, 2, 3)
graph.add_edge(1, 3, 1)
graph.add_edge(1, 4, 15)
graph.add_edge(4, 2, 7)
graph.add_edge(4, 3, 11)

graph.print_adj_matrix()


# [SECTION] Adjacency List
class Graph:
	def __init__(self, nodes, directed=True):
		self.m_directed = directed
		self.m_adj_list = {node: list() for node in nodes}

	def add_edge(self, node1, node2, weight=1):
		self.m_adj_list[node1].append((node2, weight))

		if not self.m_directed:
			if node1 == node2:
				pass
			else:
				self.m_adj_list[node1].append((node1, weight))

	def print_adj_list(self):
		for key in self.m_adj_list.keys():
			print("node", key, ": ", self.m_adj_list[key])

n = [0, 1, 2, 3, 4]

graph = Graph(n)

graph.add_edge(0, 2, 3)
graph.add_edge(0, 0, 25)
graph.add_edge(0, 1, 5)
graph.add_edge(1, 3, 1)
graph.add_edge(1, 4, 15)
graph.add_edge(4, 2, 7)
graph.add_edge(4, 3, 11)

graph.print_adj_list()