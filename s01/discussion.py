import networkx as nx 
import matplotlib.pyplot as plt

# [SECTION] Directed Graph
# Initializes a directed graph
graph = nx.DiGraph()

# Adds vertices to the graph as well as edges that connect each vertex
graph.add_edges_from([('a', 'b'), ('a', 'e'), ('b', 'c'), ('b', 'd'), ('d', 'e'), ('c', 'a')])

# Applies a positioning value that dictates the position of the graph as well as the size of the vertices and its color
positioning = nx.spring_layout(graph, seed = 500)
nx.draw_networkx(graph, pos=positioning, with_labels=True)
nx.draw_networkx_nodes(graph, positioning, node_size=300, node_color="lightgray")

# Downloads/saves the graph as an image file, then shows the graph in a window
plt.savefig('directedgraph.png')
plt.show()


# [SECTION] Undirected Graph
graph = nx.Graph()
graph.add_edges_from([('a', 'b'), ('a', 'e'), ('b', 'c'), ('b', 'd'), ('d', 'e'), ('c', 'a')])

positioning = nx.spring_layout(graph, seed=500)
nx.draw_networkx(graph, pos=positioning, with_labels=True)
nx.draw_networkx_nodes(graph, positioning, node_size=700, node_color="lightgray")

plt.savefig('undirectedgraph.png')
plt.show()